using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorDoor : MonoBehaviour
{
    Vector2 startPositoin;

    [SerializeField]
    Vector3 endPosition;
    [SerializeField]
    float step;

    float progress;

    // Update is called once per frame
    void FixedUpdate()
    {
        StartCoroutine(nameof(openDoor));
    }

    IEnumerator openDoor() 
    {
        yield return new WaitForSecondsRealtime(5f);
        transform.position = Vector3.Lerp(transform.position, endPosition, progress);
        progress += step;
    }
}
